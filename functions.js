let calculate = {};

/*==========================
	When the page loads:
	Run these functions
===========================*/
var callback = function () {
	
	/* #2: Even Fibonacci Numbers */
	document.getElementById("answer-2-even-fibonacci-numbers").innerHTML = calculate.calculateSumEvenFibonacciNumbers(4000000);

	/* #1: Multiples of 3 and 5 */
	document.getElementById("answer-1-multiples-3-5").innerHTML = calculate.calculateMultiplesOf3Or5(1000);
};

if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
	callback();
} else {
	document.addEventListener("DOMContentLoaded", callback);
}

/* #2: Even Fibonacci Numbers */
calculate.calculateSumEvenFibonacciNumbers = function(notExceed) {
	
	var count = 0;
	
	var sumOfEvenNumbers = 0;
	
	var fibonacciSequence = [1, 2];
	
	var currentNumber = 0;
	
	while(currentNumber <= notExceed){
		
		var previousNumber = fibonacciSequence[count];
	
		currentNumber = fibonacciSequence[count+1];
	
		var nextNumber = previousNumber + currentNumber;
		
		fibonacciSequence.push(nextNumber);
		
		if(currentNumber%2 == 0){
			sumOfEvenNumbers += currentNumber;
		}

		count++;
	}
	
	return sumOfEvenNumbers;
}


/* #1: Multiples of 3 and 5 */
calculate.calculateMultiplesOf3Or5 = function(toCalculate){
	var multiplesOf3Or5 = [];
	
	var sumOfMultiples = 0;
	
	// Loop through numbers below the one given as a parameter
	for(var i = toCalculate-1; i > 0; i--){
		if(i%3 == 0 || i%5 == 0){
			// Number is divisible by 3 or 5 so add to array
			// Unnecessary for this project but why not have it
			multiplesOf3Or5.push(i);
			
			// Also add to the sum total
			sumOfMultiples += i;
		}
	}
	
	return sumOfMultiples;
}


module.exports = calculate;